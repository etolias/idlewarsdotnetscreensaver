﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ScreenSaver2
{
    static class Program
    {

        static SocketHandling sh = new SocketHandling();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            sh.connect();
            Application.ApplicationExit += new EventHandler(onApplicationExit);
            if (args.Length > 0)
            {
                if (args[0].ToLower().Trim().Substring(0, 2) == "/s")
                {
                    //show the screen saver

                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    ShowScreenSaver();
                    Application.Run();

                }
                else if (args[0].ToLower().Trim().Substring(0, 2) == "/p")
                {
                    //preview the screen saver
                }
                else if (args[0].ToLower().Trim().Substring(0, 2) == "/c")
                {
                    //configure the screen saver
                    //Application.EnableVisualStyles();
                    //Application.SetCompatibleTextRenderingDefault(false);
                    //Application.Run(new ConfigureForm());
                    MessageBox.Show("This screensaver has no options that you can set", "My cool Screen Saver", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
            else
            {
                // no arguments were pased (we should probably show hte screen saver)
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                ShowScreenSaver();
                Application.Run();
            }
            
            /*Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ScreenSaverMainForm());
            */
        }
        static void ShowScreenSaver()
        {
            
            //Screen screen = Screen.AllScreens[0];
            foreach (Screen screen in Screen.AllScreens)
            {
                //ScreenSaverMainForm screensaver = new ScreenSaverMainForm(screen.WorkingArea.Height, screen.WorkingArea.Width);
                ScreenSaverMainForm screensaver = new ScreenSaverMainForm(screen);
                //screensaver.Bounds = Screen.GetBounds(screensaver);
                screensaver.Bounds = screen.Bounds;

                //screensaver.StartPosition = FormStartPosition.Manual;


                screensaver.WindowState = FormWindowState.Maximized;
                screensaver.FormBorderStyle = FormBorderStyle.None;
                screensaver.TopMost = true;
                
                screensaver.ShowImage();
                screensaver.Show();

            }

        }
        private static void onApplicationExit(object sender, EventArgs e)
        {
            Console.Out.WriteLine("=====================================================");
            Console.Out.WriteLine("onApplicationExit");
            Console.Out.WriteLine("=====================================================");
            sh.disconnect();
        }
    }
}
