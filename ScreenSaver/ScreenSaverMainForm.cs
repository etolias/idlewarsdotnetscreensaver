﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ScreenSaver2
{
    public partial class ScreenSaverMainForm : Form
    {
        
        double systemHeight;
        double systemWidth;
        string imagePath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\idlewars_image.png";
        Bitmap img;// = new Bitmap(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\idlewars_image.png");
        Byte[] buffer;
        MemoryStream stream;
        private Screen screen;
        private int mouseX;
        private int mouseY;

        public ScreenSaverMainForm(double systemHeight, double systemWidth)
        {
            
            this.systemHeight = systemHeight;
            this.systemWidth = systemWidth;
            

            InitializeComponent();
            
            timer1.Interval = 500;
            timer1.Enabled = true;
            timer1.Start();
            try
            {
                //TODO
                //buffer = File.ReadAllBytes(imagePath);
                //stream = new MemoryStream(buffer);
                //img = (Bitmap)Bitmap.FromStream(stream);
            }catch(IOException e){
                Console.WriteLine("Unable to read the file");
            }
            mouseX = Control.MousePosition.X;
            mouseY = Control.MousePosition.Y;

        }

        public ScreenSaverMainForm(Screen screen)
            : this(screen.WorkingArea.Height, screen.WorkingArea.Width)
        {
            // TODO: Complete member initialization
            this.screen = screen;
        }

        private void ScreenSaverMainForm_KeyDown(object sender, KeyEventArgs e)
        {
            Application.Exit();
        }

        private void ScreenSaverMainForm_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        Point OriginalLocation = new Point(int.MaxValue, int.MaxValue);
        
        private void ScreenSaverMainForm_MouseMove(object sender, MouseEventArgs e)
        {
            /*//see if originallocation has been set
            if (OriginalLocation.X == int.MaxValue & OriginalLocation.Y == int.MaxValue)
            {
                OriginalLocation = e.Location;
            }

            //see if the mouse has moved more than 20 pixels
            //in any direction. If it has. close the application.

            if (Math.Abs(e.X - OriginalLocation.X) > 20 | Math.Abs(e.Y - OriginalLocation.Y) > 20)
            {
                Application.Exit();
            }*/
        }

        public void ShowImage()
        {
            /*this.Bounds = screen.Bounds;
            this.StartPosition = FormStartPosition.Manual;
            this.WindowState = FormWindowState.Maximized;
            this.FormBorderStyle = FormBorderStyle.None;
            this.TopMost = true;
            */


            //this.pictureBox1.ImageLocation = "c:\\Users\\et2e10\\Pictures\\0D113D8A-3E41-46FA-AFE9-3D3D78884469.jpg";
            //this.pictureBox1.ImageLocation = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\idlewars_image.png";
            
            //this.pictureBox1.Refresh();
            //string s = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            /*this.pictureBox1.Top =(int)(systemHeight - this.pictureBox1.Height) / 2;
            this.pictureBox1.Left = (int)(systemWidth - this.pictureBox1.Width) / 2;*/
            
            try
            {
                //img = new Bitmap(imagePath);
                //this.Size = img.Size;

                Console.Out.WriteLine("Before ReadAllBytes");
                buffer = File.ReadAllBytes(imagePath);
                //buffer = ReadAllBytes(imagePath);
                Console.Out.WriteLine("After ReadAllBytes");

                stream = new MemoryStream(buffer);
                img = (Bitmap)Bitmap.FromStream(stream);


                //this.pictureBox1.Image = new Bitmap(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\idlewars_image.png");

                this.pictureBox1.Image = img;
                this.pictureBox1.ImageLocation = this.imagePath;
                this.pictureBox1.Refresh();
                this.pictureBox1.Update();
                this.pictureBox1.Size = this.pictureBox1.Image.Size;
                this.pictureBox1.Top = (int)(systemHeight - this.pictureBox1.Image.Height) / 2;
                this.pictureBox1.Left = (int)(systemWidth - this.pictureBox1.Image.Width) / 2;
            }catch (Exception e ){
                Console.Out.WriteLine(this.imagePath+"**Image NOT found....***");
            }
        }

        private void ScreenSaverMainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        public static byte[] ReadAllBytes(String path)
        {
            byte[] bytes;
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                int index = 0;
                long fileLength = fs.Length;
                if (fileLength > Int32.MaxValue)
                    throw new IOException("File too long");
                int count = (int)fileLength;
                bytes = new byte[count];
                while (count > 0)
                {
                    int n = fs.Read(bytes, index, count);
                    if (n == 0)
                    throw new InvalidOperationException("End of file reached before expected");
                    index += n;
                    count -= n;
                }
            }
        return bytes;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            if (mouseY != Control.MousePosition.Y || mouseX != Control.MousePosition.X)
            {
                Application.Exit();
            }

            mouseX = Control.MousePosition.X;
            mouseY = Control.MousePosition.Y;

            Console.Out.WriteLine("x: " + mouseX + " y: " + mouseY);

            Console.Out.WriteLine("===================timer_tick===================");
            this.ShowImage();
            Console.Out.WriteLine("==============after timer_tick==================");
            Console.Out.WriteLine(imagePath);
            //this.pictureBox1.Refresh();
            //this.pictureBox1.ImageLocation = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\idlewars_image.png";
            //this.pictureBox1.Top = (int)(systemHeight - this.pictureBox1.Height) / 2;
            //this.pictureBox1.Left = (int)(systemWidth - this.pictureBox1.Width) / 2;
            //Control.MousePosition.
            
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {

            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //Application.Exit();
        }

        private void ScreenSaverMainForm_MouseHover(object sender, EventArgs e)
        {
            //Application.Exit();
        }
    }
}
