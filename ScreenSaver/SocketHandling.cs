﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;


namespace ScreenSaver2
{
    class SocketHandling
    {
        private const int port = 9876;
        private const string address = "127.0.0.1";
        private IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(address), port);
        private Socket sender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        public void connect()
        {
            try
            {
                //Create a TCP/IP socket.
                

                sender.Connect(remoteEP);
                Console.Out.WriteLine("=================================================================");
                Console.Out.WriteLine("Socket connected to {0}", sender.RemoteEndPoint.ToString());
                Console.Out.WriteLine("=================================================================");
                //Encode the data string into a byt array



            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Problem connecting to the client application");
                Console.Out.WriteLine(e.ToString());

            }
        }

        public void disconnect(){
            try
            {
                if (sender.Connected)
                {
                    sender.Shutdown(SocketShutdown.Both);
                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Unable to shutdown sender socket");
                Console.Out.WriteLine(e.ToString());
                
            }
            try
            {
                sender.Close();
            }catch(Exception e){
                Console.Out.WriteLine("Unable to close sender socket");
                Console.Out.WriteLine(e.ToString());
            }
        }

        
    }
}
